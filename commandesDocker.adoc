== Docker

=== Docker commands
----
docker ps <1>
docker ps -a <2>
docker images <3>
----
<1> Voir les containers en cours 
<2> Voir tous les containers
<3> Voir toutes les images installées

==== Supprimer tous les containers avec le status Exited
----
docker rm $(docker ps -q -f status=exited)
----
==== Supprimer toutes les images (ces images ne doivent pas avoir de référence vers un container)
----
docker rmi $(docker images -q)
----
==== Voir les logs d'un container
----
docker logs -f <container_name>
----
==== Voir les informations d'un container
----
docker inspect <container_name>
----
==== Agir dans un container

Se connecter au bash d'un container
----
docker exec -it <CONTAINER_ID> bash
----
Passer des commandes à un container (pour aller voir la liste des hôtes par exemple)
----
docker exec -it <CONTAINER_ID> cat /etc/hosts
----
==== Lister les fichiers d'un container

Lister les containers
----
docker ps -a
----
Aller voir les fichiers d'un des containers contenu dans la liste précédente
----
sudo su
cd /var/lib/docker
ls -list  # pour voir tous les dossiers
cd /containers/<CONTAINER_ID>+<OTHER_HASH_NAME>
ls -list #pour voir tous les fichiers
----
==== Sauvegarde et restauration d'un container

Sauvegarder un container
----
docker ps -a # choisir le container à sauvegarder
docker commit -p <CONTAINER_ID> <YOUR_BACKUP_NAME> # transformer le container en image
docker images # on voit une image avec le nom <YOUR_BACKUP_NAME>
----
Sauvegarder le container en archive
----
docker save -o <CONTAINER_FILE>.tar <YOUR_BACKUP_NAME>
----
Sauvegarder ce fichier *.tar sur le PC de destination

Restaurer
----
docker load -i <CONTAINER_FILE>.tar
----
=== Docker-compose

Instancier tous les containers compris dans le fichier 
----
docker-compose up docker-compose.yml
----
Instancier tous les containers et se détacher
----
docker-compose up -d
----
Arrête et enlève tous les containers,images et links que le docker-compose a créé
----
docker-compose -f docker-compose.yml down --rmi all
----
Voir les logs des containers créés par le docker-compose.yml
----
docker-compose logs -f --tail 100
----
=== Registry

Récupération de la liste des images présentes dans le registry
----
curl http://<ip du registre>:5000/v2/_catalog?pretty
----
Récupération des tags d’une des images
----
curl http://<ip du registre>:5000/v2/<nom de l’image>/tags/list
----
=== Docker stats

Récupérer les statistiques des dockers
----
docker stats
----

Récupérer ces statisques mais avec le nom des containers
----
docker stats --format "table {{.ID}}\t{{.Name}}\t{{.CPUPerc}}\t{{.MemUsage}}\t{{.MemPerc}}\t{{.NetIO}}\t{{.BlockIO}}\t{{.PIDs}}"
----
