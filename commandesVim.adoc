== Vim

=== Déplacements


|===
|*Commande* | *Action*

|kbd:[j]
| en bas
|kbd:[k] 
|en haut
|kbd:[h] 
|à gauche
|kbd:[l] 
|à droite
|===


|===
|*Commande* | *Action*

|kbd:[^] |pour le début de ligne
|kbd:[$] |pour la fin de la ligne
|kbd:[w] |pour le début du mot suivant
|kbd:[b] |pour le début du mot précédent
|kbd:[e] |pour la fin du mot suivant
|===

|===
|*Commande* | *Action*

|kbd:[gg] |première ligne
|kbd:[G] |dernière ligne
|kbd:[3+G] |3ème ligne
|kbd:[2+w] |deuxième mot
|===

=== Copier Couper Coller

|===
|*Commande* | *Action*

|kbd:[y+y] |copie la ligne courante
|kbd:[y+2] |copier les 2 lignes
|kbd:[y+1+w] |copie le premier mot
|kbd:[p] |colle le contenu du presse papier
|kbd:[d+d] |coupe la ligne courante
|kbd:[J] |fusionne la ligne courante avec la suivante
|===

=== Commandes Shell

|===
|*Commande* | *Action*

|:!ls |liste le contenu du répertoire courant
|:r nom_du_fichier |insère le contenu du fichier nom_de_fichier dans le fichier courant
|:r! |nom_de_la_commande insère le résultat de la commande dans le fichier courant
|:r !ls |récupère le contenu du répertoire courant pour le mettre dans le fichier
|:1,$!sort |trie le texte par ordre alphabétique
|===

=== Recherche et substitution

|===
|*Commande* | *Action*

|/mot_cherché |vers le bas
|?mot_cherché |mais en partant vers le haut
|kbd:[n] |pour passer à l'itération suivante
|===

|===
|*Commande* | *Action*

|:%s/mot1/mot2/g |recherche toutes les occurences du mot1 pour les remplacer par mot2
|:s/mot1/mot2/g |recherche toutes les occurences du mot1 dans la ligne courante pour les remplacer par mot2
|:%s/mot1/mot2/gc |recherche toutes les occurences du mot1 pour les remplacer par mot2 en demandant confirmation
|===

=== Fichiers

|===
|*Commande* | *Action*

|vim fichier1 fichier2 |ouvre 2 fichiers
|:n |pour aller au fichier suivant
|:N |pour aller au fichier précédent
|:args |pour voir les fichiers ouverts
|:n nom_des_fichiers |pour ouvrir une nouvelle liste de fichiers
|===

=== Split

|===
|*Commande* | *Action*

|:split |ouvre le même fichier dans deux fenêtres
|:split fichier2 |ouvre un autre fichier dans une autre fenêtre (le tab autocomplète avec les fichiers dans le même dossier)
|:vsplit |comme le split mais en vertical
|:e |pour ouvrir un fichier
|:q |pour fermer la fenêtre
|===

=== Diff

|===
|*Commande* | *Action*

|vimdiff fichier1 fichier2 |pour comparer deux fichiers
|vimget |pour récupérer les modifications sur le fichier courant
|vimput |pour pousser les modifications à partir du fichier courant
|===

=== Folding

|===
|*Commande* | *Action*

|kbd:[z+R] |pour déplier tout le fichier
|kbd:[z+C] |pour replier tout le fichier
|===

=== Autres commandes utiles

|===
|*Commande* | *Action*

|kbd:[g+g+=+G] |réindente tout le texte du fichier
|kbd:[u] |annule la dernière commande
|kbd:[Ctrl+R] |annule la dernière annulation
|===

|===
|*Commande* | *Action*

|:set nu |mets le numéro des lignes
|:set nu! |les enlève
|===